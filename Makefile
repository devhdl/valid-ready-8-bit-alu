CWD=$(shell pwd)
SIM ?= icarus
VERILOG_SOURCES =$(CWD)/alu/rtl/alu_rtl.v
MODULE := alu.testbench.test.alu_always_valid_test
TOPLEVEL := alu_rtl
TOPLEVEL_LANG=verilog
COCOTB_HDL_TIMEUNIT=1ns
COCOTB_HDL_TIMEPRECISION=1ns
include $(shell cocotb-config --makefiles)/Makefile.sim
