"""
    This file stores the simple implementation of the calss Logic.
"""


class Logic:
    """
        Logic is a class that stores the name, number of bits (or width),
        direction and the label of the object. This allows user to pass in
        information about the signals of the DUT without coding them inside
        the agents.

        Attributes
        ----------
            direction : str
                This is the direction of the signal. It can only be 'Input',
                'Output' and 'Inout'
            name : str
                This is the name of the signal in the RTL code.
            bits : integer
                This is the width of the signal bus. Default is 1.
            label : str
                This is the group the signal belongs to. Default is 'None'.
    """

    def __init__(self, direction, signal_name, bits=1, label='None') -> None:
        """
            Constructor of the class Logic.

            Parameters
            ----------
                direction : str
                    This is the direction of the signal. It can only be 'Input',
                    'Output' and 'Inout'
                signal_name : str
                    This is the name of the signal in the RTL code.
                bits : integer
                    This is the width of the signal bus. Default is 1.
                label : str
                    This is the group the signal belongs to. Default is 'None'.

            Returns
            -------
                None
        """
        self.name = signal_name
        self.bits = bits
        self.label = label
        self.direction = direction
        
    def __call__(self, dut):
        """
            If the object is called, we return the signal object from the DUT.
            This is specifically used when we want to get or drive the Logic
            through the COCOTB API.

            Parameters
            ----------
                dut : dut
                    dut is a dut object from COCOTB. This object consists of
                    all the signal information about the dut and allow us to
                    drive or read the value of the signals.

            Returns
            -------
                signal : cocotb signal in DUT
                    This returns a signal object in COCOTB from the DUT. This
                    object can be used when we want to read or drive the signal.
        """
        signal = getattr(dut, self.name)
        return signal

