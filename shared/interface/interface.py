"""
    This file stores the simple implementation of the class Interface.
"""

from logic import Logic


class Interface:
    """
        Interface is a class that stores a group of logic signals. This
        class is used by agents to obtain specific signal name, which
        allows agent implementation to be generic.

        Attributes
        ----------
            signals : list
                A list of signals of type Logic.
            label_dict : dict
                A dictionary that groups logic by their labels.
    """
    
    def __init__(self, *argv) -> None:
        """
            Constructor of class Interface.

            Parameters
            ----------
                *argv : Logic
                    Any number of logic depending on how many signals
                    the target interface consists.
            
            Returns
            -------
                None.
        """
        self.signals = []
        self.label_dict = {}

        for arg in argv:
            if (type(arg) is Logic):
                self.signals.append(arg)
            else:
                raise ValueError("Argument of an interface must be a logic.")

        for signal in self.signals:
            if signal.label in self.label_dict:
                self.label_dict[signal.label].append(signal)
            else:
                self.label_dict[signal.label] = [signal]

    def __call__(self) -> dict:
        """ 
            When the object is called, it will return the dictionary sorted
            by labels.
        """
        return self.label_dict

