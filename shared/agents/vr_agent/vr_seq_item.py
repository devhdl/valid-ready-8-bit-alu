"""
    This files stores the implementation of a generic Vr sequence item.
"""

import random
from pyuvm import uvm_sequence_item
from vr_payload import VrPayload


class VrSeqItem(uvm_sequence_item):
    """
        VrSeqItem is a generic uvm_sequence_item that stores transactions
        through a targeted Vr interface. This agent is generic and requires
        a specified interface.

        This class is based on uvm_sequence_item. All the feature of an
        uvm_sequence_item in this class.

        Attributes
        ----------
            valid : int
                An integer between 0 and 1 to define whether valid is high
                or low.
            ready : int
                An integer between 0 and 1 to define whether ready is high
                or low.
            payload : VrPayload
                An VrPayload object that stores the values of the signals.
            valid_rand_en : bool
                If True, value of valid will be randomised.
            ready_rand_en : bool
                If True, value of ready will be randomised.
            payload_rand_en : bool
                If True, payload will be randomised.
    """

    def __init__(self, name, interface=None) -> None:
        """
            Constructor of VrSeqItem class.

            Parameters
            ----------
                name : str
                    The specific name of the vr seq item.
                interface : Interface
                    This is an Interface object that stores the
                    essential information of the signals of the targeted
                    interface.

            Returns
            -------
                None.

            Notes
            -----
                For more detail, see uvm_sequence_item in UVM and pyuvm docs.
            
        """
        super().__init__(name)
        self.valid = None
        self.ready = None
        if interface is not None:
            try:
                self.payload = VrPayload(signals=interface['payload'])
            except:
                raise ValueError("Interface of AXI seq item must have a payload.")
        else:
            self.payload = VrPayload()

        self.valid_rand_en = True
        self.ready_rand_en = True
        self.payload_rand_en = True

    def randomize(self):
        """
            A function that randomises the valid, ready and payload, depending
            on whether their rand_en is high.
        """
        if self.valid_rand_en:
            self.valid = random.randint(0, 1)
        
        if self.ready_rand_en:
            self.ready = random.randint(0, 1)
        
        if self.payload_rand_en:
            self.payload.randomize()

    def __str__(self) -> str:
        """
            When str(VrSeqItem) is called, we will get the detail information
            about the current value of valid, ready and payload.

            Parameters
            ----------
                None.

            Returns
            -------
                output : str
                    The information of the values of valid, ready and payload
                    as a string.
        """
        output = f"[{self.get_name()}]:\n"
        output = output + f"Valid: {self.valid}\n"
        output = output + f"Ready: {self.ready}\n"
        output = output + str(self.payload)

        return output

