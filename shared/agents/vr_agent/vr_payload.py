"""
    This files stores the implementation of the Vr payload class.
"""

import random


class VrPayload:
    """
        Payload is a class that stores the information of the payload.
        It is also responsible for randomising the fields and setting
        up the constraint.

        Attributes
        ----------
            payload : dict
                A dictionary with all the logic signal values.
            payload_constraint : dict
                A dictionary with all the logic signal constraint.
                By default, it will be from 0 to 2**number_of_bits - 1.
    """

    def __init__(self, signals=None) -> None:
        """
            Constructor of Payload class.

            Parameters
            ----------
                signal : list
                    A list of Logic that forms the payload.

            Returns
            -------
                None.
        """

        # Setting up empty payload
        self.payload = {}

        # Setting up empty constraint
        self.payload_constraint = {}

        # Building payload and default constraint
        if signals is not None:
            for signal in signals:
                self.payload[signal.name] = None
                self.payload_constraint[signal.name] = (0, 2**signal.bits - 1)

    def set_payload(self, payload):
        """
            Setting the payload by overriding the payload dict build in
            the constructor. This should be used only when we but interface=None.

            Parameters
            ----------
                payload : dict
                    A dictionary overriding the self.payload built in the
                    constructor if it is built.

            Returns
            -------
                None.
        """
        self.payload = payload

    def set_constraint(self, key, constraint) -> None:
        """
            A setter to set constraint of a target logic signal.

            Parameters
            ----------
                key : str
                    The name of the Logic we are targeting.
                constraint : tuple
                    A tuple that defines the min max of the range of allowed
                    random. The format is: (min, max) 

            Returns
            -------
                None.
        """
        try:
            self.payload_constraint[key] = constraint
        except ValueError:
            pass

    def randomize(self) -> None:
        """
            A function that randomise the payload values.
        """
        for key in self.payload:
            constraint = self.payload_constraint[key]
            self.payload[key] = random.randint(constraint[0], constraint[1])

    def get_payload(self) -> dict:
        """
            A getter to get the payload dict.

            Parameters
            ----------
                None.

            Returns
            -------
                self.payload : dict
                    The dictionary that stores the payload values.
        """
        return self.payload

    def __str__(self) -> str:
        """
            If str(payload) is called, it will return a string
            with all the information of the payload.

            Parameters
            ----------
                None.

            Returns
            -------
                output : str
                    The string output of the information of the
                    payload.
        """
        output = "payload :\n"
        for key in self.payload:
            output = output + f"  {key}: {self.payload[key]}"

        return output

