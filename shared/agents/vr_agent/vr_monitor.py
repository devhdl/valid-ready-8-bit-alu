"""
    This file stores the implementation of a generic Vr monitor.
"""

from cocotb.triggers import RisingEdge
from pyuvm import uvm_component, uvm_analysis_port, ConfigDB
from vr_seq_item import VrSeqItem


class VrMonitor(uvm_component):
    """
        VrMonitor is a monitor that drives an Axi interface.
        More specifically, any interface with a valid, ready and a payload.
        This monitor is generic and will require a specified interface.

        This class is based on uvm_component. All features of an uvm_component
        exists in this class.

        Attributes
        ----------
            vif : Interface
                An Interface object that stores all the basic signal
                information.
            valid_signal : Logic
                A Logic object that stores all the signal information with
                label='valid'. There should only be 1 valid signal per vif.
            ready_signal : Logic
                A Logic object that stores all the signal information with
                label='ready'. There should only be 1 ready signal per vif.
            payload : list
                A list of Logic that forms the payload.
    """

    def __init__(self, name, parent, interface) -> None:
        """
            Constructor of VrMonitor class.

            Parameters
            ----------
                name : str
                    The specific name of the monitor.
                parent : uvm class
                    This is the parent object of this monitor. Most
                    likely the parent will be an VrAgent.
                interface : Interface
                    This is an Interface object that stores the
                    essential information of the signals of the targeted
                    interface.

            Returns
            -------
                None.

            Notes
            -----
                For more detail, see uvm_component in UVM and pyuvm docs.
        """
        super().__init__(name, parent)
        self.vif = interface
        self.valid_signal = self.vif()['valid'][0]
        self.ready_signal = self.vif()['ready'][0]
        self.payload = self.vif()['payload']

    def build_phase(self) -> None:
        """
            This function builds the monitor when UVM is running on its
            build phase.
        """
        super().build_phase()
        self.aport = uvm_analysis_port("aport", self)
        self.testbench = ConfigDB().get(self, "", "TB")
        self.dut = self.testbench.dut

    async def run_phase(self) -> None:
        """
            This is a task that runs during the UVM run_phase. This task
            will be responsible for monitoring the signals in the target
            interface.
        """
        while True:
            await RisingEdge(self.dut.clk_i)

            valid = get_int(self.valid_signal(self.dut))
            ready = get_int(self.ready_signal(self.dut))

            if (valid and ready):
                payload = {}

                for signal in self.payload:
                    signal_val = get_int(signal(self.dut))
                    payload[signal.name] = signal_val

                # Create and set transaction item
                transaction_item = VrSeqItem(f"{self.get_name()}_transaction")
                transaction_item.valid = valid
                transaction_item.ready = ready
                transaction_item.payload.set_payload(payload)

                self.logger.debug(f"\n[{self.get_name()}]Transaction Item is:\n{str(transaction_item)}")
                
                # Write to aport
                self.aport.write(transaction_item)


def get_int(signal) -> int:
    """
        This is a simple static helper function that will be used when reading
        signal values.

        Parameters
        ----------
            signal : COCOTB signal object
                A signal that we want to read.

        Returns
        -------
            sig : int
                The value of the signal in decimal.
    """
    try:
        sig = int(signal.value)
    except ValueError:
        sig = 0
    return sig

