"""
    This file stores the implementation of a generic Vr agent.
"""

from vr_driver import VrDriver
from vr_monitor import VrMonitor
from pyuvm import uvm_sequencer, uvm_agent, ConfigDB


class VrAgent(uvm_agent):
    """
        VrAgent is an agent that deals with Axi interfaces. More
        specifically, an interface with a valid, ready and a payload.
        This agent is generic and requires a specified interface.

        This class is based on uvm_agent. All the feature of an uvm_agent
        exists in this class.

        Attributes
        ----------
            vif : Interface
                An Interface object that stores all the basic signal
                information.
    """

    def __init__(self, name, parent, interface) -> None:
        """
            Constructor of VrAgent class.

            Parameters
            ----------
                name : str
                    The specific name of the agent.
                parent : uvm class
                    This is the parent object of this agent. Most
                    likely the parent will be an uvm_env.
                interface : Interface
                    This is an Interface object that stores the
                    essential information of the signals of the targeted
                    interface.

            Returns
            -------
                None.

            Notes
            -----
                For more detail, see uvm_agent in UVM and pyuvm docs.
        """
        super().__init__(name, parent)
        self.vif = interface

    def build_phase(self) -> None:
        """
            This function builds the agent when UVM is running on
            its build phase.
        """
        super().build_phase()
        name = self.get_name()
        self.sequencer = uvm_sequencer(name + "_seqr", self)
        ConfigDB().set(None, "*", name + "_SEQR", self.sequencer)
        self.driver = VrDriver(name + "_driver", self, self.vif)
        self.monitor = VrMonitor(name + "_monitor", self, self.vif)

    def connect_phase(self) -> None:
        """
            This function connects the ports inside the agents.
        """
        self.driver.seq_item_port.connect(self.sequencer.seq_item_export)

