"""
    This files stores the implementation of a generic Vr driver. 
"""

from pyuvm import uvm_driver, ConfigDB
from cocotb.triggers import RisingEdge


class VrDriver(uvm_driver):
    """
        VrDriver is a driver that drives an Axi interface.
        More specifically, any interface with a valid, ready and a payload.
        This driver is generic and will require a specified interface.

        This class is based on uvm_driver. All features of an uvm_driver
        exists in this class.

        Attributes
        ----------
            vif : Interface
                An Interface object that stores all the basic signal
                information.
            valid_signal : Logic
                A Logic object that stores all the signal information with
                label='valid'. There should only be 1 valid signal per vif.
            ready_signal : Logic
                A Logic object that stores all the signal information with
                label='ready'. There should only be 1 ready signal per vif.
            payload : list
                A list of Logic that forms the payload.
    """

    def __init__(self, name, parent, interface) -> None:
        """
            Constructor of VrDriver class.

            Parameters
            ----------
                name : str
                    The specific name of the driver.
                parent : uvm class
                    This is the parent object of this driver. Most
                    likely the parent will be an VrAgent.
                interface : Interface
                    This is an Interface object that stores the
                    essential information of the signals of the targeted
                    interface.

            Returns
            -------
                None.

            Notes
            -----
                For more detail, see uvm_driver in UVM and pyuvm docs.
        """
        super().__init__(name, parent)
        self.vif = interface
        self.valid_signal = self.vif()['valid'][0]
        self.ready_signal = self.vif()['ready'][0]
        self.payload = self.vif()['payload']

    def build_phase(self) -> None:
        """
            This function builds the driver when UVM is running on its
            build phase.
        """
        super().build_phase()
        self.testbench = ConfigDB().get(self, "", "TB")
        self.dut = self.testbench.dut

    async def run_phase(self) -> None:
        """
            This is a task that runs during the UVM run_phase. This task
            will be responsible for driving the signals accoridng to the
            received sequence item from the sequencer.
        """
        while True:
            seq_item = await self.seq_item_port.get_next_item()

            await RisingEdge(self.dut.clk_i)

            if self.valid_signal.direction == 'input':
                self.valid_signal(self.dut).value = seq_item.valid 

            if self.ready_signal.direction == 'input':
                self.ready_signal(self.dut).value = seq_item.ready 

            for signal in self.payload:
                if signal.direction == 'input':
                    signal(self.dut).value = seq_item.payload.get_payload()[signal.name]

            self.seq_item_port.item_done()

