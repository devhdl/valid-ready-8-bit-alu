# Valid-Ready 8-bit ALU Design
This repository is a practice playground aimed at familiarising myself with basic verilog design and verification tools. These includes:
- An 8-bit ALU design written in Verilog.
- Formal verification with SymbiYosys
- Constructing testbenches with cocotb and the pyuvm framework
- Constructing testbenches with Systemverilog and UVM framework (In the future...)

For more details see their websites:
- https://symbiyosys.readthedocs.io/en/latest/index.html
- https://docs.cocotb.org/en/stable/quickstart.html
- https://github.com/pyuvm/pyuvm

# Module
In this repository, we have an 8-bit ALU, which takes in 2 8-bit data inputs, 
denoted as data1 and data2, and a 2-bit operation select, denoted as op.
The 2-bit operation select allows 4 operations:
- `00` ADD
- `01` AND
- `10` OR
- `11` XOR

This module is connected to a clock and an synchronous active high reset.
During reset, all flip-flops inside that module will be set to zero.

The input and output interface uses a basic Valid-Ready interface, which performs
valid-ready handshakes with the upstream component and downstream component.

The rtl design of this alu module is located in the `\alu\rtl` folder.

# Testbench and usage
## Formal Verification with SymbiYosys
In the `alu_rtl.v` implementation, we have included a simple test 
that will verify that our design will not enter any bad states.

A file called `alu_rtl.sby` will be required to use the SymbiYosys tools. 
Settings such as tests, solver and proof methods will be set in this file.

To run the SymbiYosys, we simply go to the `/code` level, and run:

`$ sby -f alu_rtl.sby`

This will show that our design passed the test proven by the k-induction method.

TODO: Add more formal checks to verify Valid-Ready protocol is always followed.

## cocotb and pyUVM Testbench
A pyUVM, cocotb testbench has been implemented. This testbench consists of a generic VrAgent which will be used to create 2 specific Alu Agents. These agents monitor the input and output of the Alu. A scoreboard is also implemented to check the results.

To run the cocotb test bench, we need to make the `Makefile`. Before this, we also need to add the required python directories to the PYTHONPATH. This can be done by (in top level dir):
`$ source setup`

Once that is added to the python path of your terminal session, you just need to do make (also in top level dir).
`$ make`

