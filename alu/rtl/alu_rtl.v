`default_nettype none

module alu_rtl (
  // Input CLK and RST
  input wire       clk_i, 
  input wire       rst_i, // Active high reset

  // Upstream AXI interface
  input wire [7:0] data1_i,
  input wire [7:0] data2_i,
  input wire [1:0] op_i,
  input wire       up_valid_i,
  output reg       up_ready_o,

  // Downstream AXI interface
  input wire       dn_ready_i,
  output reg       dn_valid_o,
  output reg [7:0] result_o 
  );

  // Internal signals
  reg [7:0] result_r;
  reg [7:0] data1_r;
  reg [7:0] data2_r;
  reg       valid_r;
  reg       data_bus_en;

  // Enum for operations
  localparam add_c = 2'b00;
  localparam and_c = 2'b01;
  localparam or_c  = 2'b10;
  localparam xor_c = 2'b11;

  // RTL design...
  // Data-bus Enable Flip-flop
  always @ (posedge clk_i) begin
    if (rst_i)
      data_bus_en <= 1'b0;
    else begin
      case (data_bus_en)
        1'b1 : begin
          if (~dn_ready_i)
            data_bus_en <= 1'b0;
        end
        1'b0 : begin
          if (dn_ready_i)
            data_bus_en <= 1'b1;
        end
      endcase
    end
  end

  // Ready/Valid-buffer/Data-buffer Flip-flop
  always @ (posedge clk_i) begin
    if (rst_i) begin
      up_ready_o <= 0;
      valid_r <= 0;
      data1_r <= 0;
      data2_r <= 0;
    end
    else begin
      up_ready_o <= dn_ready_i;
      if (data_bus_en) begin
        valid_r <= up_valid_i;
        data1_r <= data1_i;
        data2_r <= data2_i;
      end
    end
  end

  // Result Flip-flop
  always @ (posedge clk_i) begin
    if (rst_i)
      result_o <= 0;
    else begin
      if (data_bus_en) begin
        if (dn_ready_i) begin
          case (op_i)
            add_c : result_o <= data1_i + data2_i;
            and_c : result_o <= data1_i & data2_i;
            or_c  : result_o <= data1_i | data2_i;
            xor_c : result_o <= data1_i ^ data2_i;
          endcase

          dn_valid_o <= up_valid_i;
        end
      end
      else if (~data_bus_en) begin
        if (dn_ready_i) begin
          case (op_i)
            add_c : result_o <= data1_r + data2_r;
            and_c : result_o <= data1_r & data2_r;
            or_c  : result_o <= data1_r | data2_r;
            xor_c : result_o <= data1_r ^ data2_r;
          endcase

          dn_valid_o <= valid_r;
        end
      end
    end
  end


// Simulation Macros
`ifdef COCOTB_SIM
  initial begin
    $dumpfile("alu_rtl.vcd");
    $dumpvars(0, alu_rtl);
    #1;
  end
`endif


`ifdef FORMAL
  initial
    assume (!rst_i);

  always @ (posedge clk_i) begin
    
    assert (result_o < 256);

    assume (!rst_i);

    if (rst_i) begin
      assert (result_o == 0);
    end
  end
`endif //  `ifdef FORMAL
endmodule // alu_rtl
