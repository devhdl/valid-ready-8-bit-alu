"""
    This file contains the test sequence for always valid test.
"""

import cocotb
from pyuvm import uvm_sequence, ConfigDB
from alu_seq_lib import SingleAlwaysReadyRandomSeq, SingleAlwaysValidRandomSeq
from alu_interface_type import alu_input_if, alu_output_if


class AluAlwaysValidTestSeq(uvm_sequence):
    """
        AluAlwaysValidTestSeq is the test sequence that create transactions for the ALU DUT.
        In this test sequence, we generate valid transaction from the upstream manager and 
        ready transaction from the downstream subordinate.
    """

    async def body(self):
        """
            This is a task that will be run when the test sequence is called. We generate
            num_iteration of transactions and put them on the respective sequencer.
        """
        alu_input_seqr = ConfigDB().get(None, "", "alu_input_agent_SEQR")
        alu_output_seqr = ConfigDB().get(None, "", "alu_output_agent_SEQR")

        num_iteration = 100
        
        for _ in range(num_iteration):
            input_seq = SingleAlwaysValidRandomSeq("always_valid_seq", interface=alu_input_if)
            output_seq = SingleAlwaysReadyRandomSeq("always_ready_seq", interface=alu_output_if)
            await input_seq.start(alu_input_seqr)
            cocotb.start_soon(output_seq.start(alu_output_seqr))
        
