"""
    This file contains the Alu TB class. This class is used by UVM testbenches
    to access the DUT and is used to create the clock.
"""

import cocotb
import logging
from cocotb.triggers import RisingEdge
from cocotb.clock import Clock


# Basic logger setting
logging.basicConfig(level=logging.NOTSET)
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


class AluTB:
    """
        AluTB class is responsible for storing the DUT and driving the clock/rst.

        Attributes
        ----------
            dut : COCOTB DUT object
                This is the handle of the DUT.
            end_with_outstanding_result : bool
                This can be set by the test to decide whether we permit an
                outstanding result in the model. We can ensure the simulation
                to end without outstanding result by adding some extra cycles
                when the simulation ends with valid low input. Then, we just
                need to have a cycle where the subordinate ready is high and it
                accepts the transaction to remove the pending result stored
                in the ALU.
    """

    def __init__(self, dut) -> None:
        """
            Constructor of AluTB class.

            Parameters
            ----------
                dut : COCOTB DUT object
                    This is the handle of the DUT.

            Returns
            -------
                None.
        """
        self.dut = dut
        self.end_with_outstanding_result = True

    async def reset (self):
        """
            A task that will toggle the reset on for 1 clock cycle. Note that this
            is designed for active high reset.
        """
        await RisingEdge(self.dut.clk_i)
        self.dut.rst_i.value = 1
        await RisingEdge(self.dut.clk_i)
        self.dut.rst_i.value = 0

    async def start_clock(self, time=200, units='ns'):
        """
            A task that start the clock of the testbench.
        """
        cocotb.start_soon(Clock(self.dut.clk_i, time, units=units).start())

