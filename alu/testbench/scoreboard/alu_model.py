"""
    This file contains the model of this ALU.

    TODO: Model the ALU under reset.
"""


class AluModel:
    """
        AluModel models the behaviour of the ALU. At the moment, AluModel
        offers 3 CHK:
            ALU_CHK1 : Received input when output is not yet accepted.
                This will be raised when the ALU has an outstanding output
                waiting for acceptance, thus not ready to receive new inputs,
                but received new inputs.
            ALU_CHK2 : Expecting output when there are no valid results.
                This will be raised when the ALU should have an outstanding
                output but none could be found.
            ALU_CHK_3 : Result output does not match expected.
                This will be raised when the model input does not match the
                RTL output.

        It also offers 1 optional CHK:
            ALU_OPCHK1 : A valid result is ready to be accepted by the subordinate
                but was never accpeted by the subordinate when simulation ends.
                This CHK is only used at the end of simulation. This is optional
                as we allow simulations to end aruptly without setting the
                input transaction valid low for a couple of cycles before sim
                ends. To enable this CHK, please set end_with_outstanding_result
                in alu_tb false.

        Attributes
        ----------
            pending_result : int
                When input is accpeted, we store the expected output here. This
                will be stored here until an accepted output transaction is
                observed. We will check if they match and after the check, it
                will be set to None.
    """

    def __init__(self) -> None:
        """
            The constructor of the ALU model.
        """
        self.pending_result = None

    def check_pending_clear(self) -> tuple:
        """
            This checks whether we are accepting input when we haven't released
            an outstanding output.
        """
        if self.pending_result is not None:
            msg = "ALU_CHK_1 : "
            msg = msg + "Received input when output is not yet accepted."
            return False, msg
        else:
            return True, "PASS"

    def set_pending_result(self, input_seq_item) -> tuple:
        """
            This accepts a valid input to the ALU, calculates the expected
            output and stores it in the pending_result.

            Parameters
            ----------
                input_seq_item : AxiSeqItem
                    This is the sequence item that stores the data of the
                    input to the ALU.
        """
        success, msg = self.check_pending_clear()
        if success is False:
            return False, msg 
        else:
            op = input_seq_item.payload.payload['op_i'] 
            data1 = input_seq_item.payload.payload['data1_i']
            data2 = input_seq_item.payload.payload['data2_i']

            if op == 0:
                self.pending_result = (data1 + data2) % (2 ** 8)
            elif op == 1:
                self.pending_result = data1 & data2
            elif op == 2:
                self.pending_result = data1 | data2
            elif op == 3:
                self.pending_result = data1 ^ data2

            return True, "PASS"

    def check_result(self, output_seq_item) -> tuple:
        """
            This accepts a valid output and check if it matches the expected
            result calculated by the accepted input.

            Parameters
            ----------
                output_seq_item : AxiSeqItem
                    This is the sequence item that stores the result of the input.
        """
        if self.pending_result is None:
            msg1 = "ALU_CHK_2 : "
            msg1 = msg1 + "Expecting output when there are no valid results."
            return False, msg1
        else:
            result_output = output_seq_item.payload.payload['result_o']
            if self.pending_result == result_output:
                self.pending_result = None
                return True, "PASS"
            else:
                msg2 = "ALU_CHK_3 : "
                msg2 = msg2 + f"Result output {result_output} does not match expected {self.pending_result}."
                self.pending_result = None
                return False, msg2

    def check_end_of_sim(self):
        """
            This function is used to check if there is any error when simulation
            ends.
        """
        if self.pending_result is None:
            return True, "PASS"
        else:
            return False, "ALLU_OPCHK1 : An outstanding result is not accepted by the subordinate."

