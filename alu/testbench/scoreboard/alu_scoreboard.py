"""
    This file contains the implementation of the scoreboard the checks the behaviour
    of the ALU.
"""

from alu_model import AluModel
from pyuvm import uvm_tlm_analysis_fifo, uvm_get_port, uvm_scoreboard, ConfigDB
from cocotb.triggers import RisingEdge


class AluScoreboard(uvm_scoreboard):
    """
        AluScoreboard is the scoreboard that checks the behaviour of the ALU.
        It also stores the statistics of the transaction types and number of failures.

        Attributes
        ----------
            accepted_transaction : int
                Number of transactions accepted by the ALU.
            no_of_add : int
                Number of transaction with operation Add accepted by the ALU.
            no_of_and : int
                Number of transaction with operation And accepted by the ALU.
            no_of_or : int
                Number of transaction with operation Or accepted by the ALU.
            no_of_xor : int
                Number of transaction with operation Xor accepted by the ALU.
            no_of_fail : int
                Number of failures detected by the scoreboard during the simulation. 
            no_of_after_sim_fail : int
                Number of failures detected by the scoreboard after the simulation.
            first_error : str
                The error msg of the first encountered error.
    """

    def __init__(self, name, parent):
        """
            The constructor of the scoreboard.

            Parameters
            ----------
                name : str
                    The name of the scoreboard instance.
                parent : uvm_env
                    The parent class of the scoreboard.
        """
        super().__init__(name, parent)
        self.accepted_transaction = 0
        self.no_of_add = 0
        self.no_of_and = 0
        self.no_of_or = 0
        self.no_of_xor = 0

        self.no_of_fail = 0
        self.no_of_after_sim_fail = 0

        self.first_error = None

    def error(self, msg):
        """
            This is a helper function that output the error with the logger
            and if this is the first error encountered, it will record this
            down.

            Parameters
            ----------
                msg : str
                    The error message.
        """
        self.logger.error(msg)

        if self.first_error is None:
            self.first_error = msg

    def build_phase(self):
        """
            This builds the scoreboard when UVM is in its build phase.
        """
        self.alu_input_export_fifo = uvm_tlm_analysis_fifo("alu_input_export_fifo", self)
        self.alu_input_get_port = uvm_get_port("alu_input_get_port", self)
        self.alu_input_export = self.alu_input_export_fifo.analysis_export
        
        self.alu_output_export_fifo = uvm_tlm_analysis_fifo("alu_output_export_fifo", self)
        self.alu_output_get_port = uvm_get_port("alu_output_get_port", self)
        self.alu_output_export = self.alu_output_export_fifo.analysis_export
        
        self.model = AluModel()
        self.success = True

        self.testbench = ConfigDB().get(self, "", "TB")
        self.dut = self.testbench.dut

    def connect_phase(self):
        """
            This connects the ports with the fifo exports.
        """
        self.alu_input_get_port.connect(self.alu_input_export_fifo.get_export)
        self.alu_output_get_port.connect(self.alu_output_export_fifo.get_export)

    async def run_phase(self):
        """
            This checks if the transactions are correct in the run_phase.
        """

        while True:
            await RisingEdge(self.dut.clk_i)

            input_received, input_transaction = self.alu_input_get_port.try_get()
            output_received, output_transaction = self.alu_output_get_port.try_get()

            self.logger.debug(f"{input_received}, {str(input_transaction)}")
            self.logger.debug(f"{output_received}, {str(output_transaction)}")

            if output_received:
                result_pass, msg1 = self.model.check_result(output_transaction)

                if not(result_pass):
                    self.success = False
                    self.no_of_fail = self.no_of_fail + 1
                    self.error(msg1)

            if input_received:
                input_pass, msg2 = self.model.set_pending_result(input_transaction)

                self.accepted_transaction = self.accepted_transaction + 1

                op = input_transaction.payload.payload['op_i']
                if op == 0:
                    self.no_of_add = self.no_of_add + 1
                elif op == 1:
                    self.no_of_and = self.no_of_and + 1
                elif op == 2:
                    self.no_of_or = self.no_of_or + 1
                elif op == 3:
                    self.no_of_xor = self.no_of_xor + 1

                if not(input_pass):
                    self.success = False
                    self.no_of_fail = self.no_of_fail + 1
                    self.error(msg2)

    def check_phase(self):
        """
            This checks if there are any issues in the model after simulation
            ends.
        """ 
        no_pending_result, msg = self.model.check_end_of_sim()
        
        if no_pending_result:
            self.logger.debug("Model exited simulation clean.")
        else:
            if self.testbench.end_with_outstanding_result:
                warning = f"\nError Waived : {msg}\n"
                warning = warning + "Reason : This test did not require"
                warning = warning + " the model to end without outstanding result."

                self.logger.warning(warning)
            else:
                self.error(msg)
                self.no_of_after_sim_fail = self.no_of_after_sim_fail + 1
                self.success = False


    def report_phase(self):
        """
            This performs the final report on the statistics and return error
            if failures are found.
        """
        self.logger.info(f"\n\nReporting {self.get_name()} statistics\n" +

            f"No of transactions: {self.accepted_transaction}\n" +
            f"No of ADD: {self.no_of_add}\n" +
            f"No of AND: {self.no_of_and}\n" +
            f"No of OR : {self.no_of_or}\n" +
            f"No of XOR: {self.no_of_xor}\n" +
            "\n" +
            f"No of runtime FAIL: {self.no_of_fail}\n" +
            f"No of after sim FAIL: {self.no_of_after_sim_fail}")

        assert self.success, f"[{self.get_name()}] Failed Check detected.\nError is: {self.first_error}"

