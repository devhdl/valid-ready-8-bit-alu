"""
    This file contains the test structure for always valid test.
"""

import cocotb
from pyuvm import uvm_test, ConfigDB, uvm_root
from alu_env import AluEnv
from alu_always_valid_test_seq import AluAlwaysValidTestSeq
from alu_tb import AluTB


class AluAlwaysValidTest(uvm_test):
    """
        AluAlwaysValidTest is a test that will always generate valid
        transaction from the manager and generate ready transaction from
        the downstream subordinate.
    """

    def build_phase(self):
        """
            This function builds the test by creating the tb, ConfigDB and
            the environment.
        """
        self.tb = AluTB(cocotb.top)
        ConfigDB().set(None, "*", "TB", self.tb)
        self.env = AluEnv("alu_env", self)

    async def run_phase(self):
        """ 
            This task is run when UVM is in its run_phase. Here we create the
            test_seq, start the clock, reset the DUT, then start the test_seq.
        """
        self.test_seq = AluAlwaysValidTestSeq("alu_traffic_test_seq")
        self.raise_objection()
        await self.tb.start_clock()
        self.logger.info(f"[{self.get_name()}] Clock started.")
        cocotb.start_soon(self.tb.reset())
        self.logger.info(f"[{self.get_name()}] Out of reset.")
        await cocotb.start_soon(self.test_seq.start())
        self.drop_objection()


"""
    Cocotb test Macro.
"""
@cocotb.test()
async def alu_traffic_test(_):
    await uvm_root().run_test("AluAlwaysValidTest")

