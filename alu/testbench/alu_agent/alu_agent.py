"""
    This files stores ALU agents that is based on generic
    Vr agents with appropriate interface.
"""

from alu_interface_type import alu_input_if, alu_output_if
from vr_agent import VrAgent


class AluInputAgent (VrAgent):
    """
        This agent is responsible for driving and monitoring
        input interface of the ALU.
    """
    def __init__(self, name, parent):
        interface = alu_input_if
        super().__init__(name, parent, interface)


class AluOutputAgent (VrAgent):
    """
        This agent is responsible for driving and monitoring
        output interface of the ALU.
    """
    def __init__(self, name, parent):
        interface = alu_output_if
        super().__init__(name, parent, interface)

