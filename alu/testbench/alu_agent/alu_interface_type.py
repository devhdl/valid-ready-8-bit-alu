"""
    This file stores the interfaces of our ALU DUT.
"""

from interface import Interface
from logic import Logic


alu_input_if = Interface(
    Logic('input', 'data1_i', bits=8, label='payload'),
    Logic('input', 'data2_i', bits=8, label='payload'),
    Logic('input', 'op_i', bits=2, label='payload'),
    Logic('input', 'up_valid_i', label='valid'),
    Logic('output', 'up_ready_o', label='ready')
)

alu_output_if = Interface(
    Logic('output', 'result_o', bits=8, label='payload'),
    Logic('output', 'dn_valid_o', label='valid'),
    Logic('input', 'dn_ready_i', label='ready')
)

