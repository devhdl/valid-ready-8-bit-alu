from vr_seq_item import VrSeqItem
from pyuvm import uvm_sequence


class SingleAlwaysValidRandomSeq(uvm_sequence):
    """
        This is a basic sequence that create Vr sequence item that
        is always valid with random payload. We are not concerned
        about the ready signal since that should be generated by the
        DUT.

        Attributes
        ----------
            vif : Interface
                The target interface.

        Notes
        -----
            See more about uvm_sequence in UVM and pyuvm doc.
    """

    def __init__(self, name, interface):
        """
            Constructor of always valid sequence.

            Parameters
            ----------
                name : str
                    The name of the instance of this sequence.
                interface : Interface
                    An Interface object that stores the Logic information
                    of the targeted interface.
        """
        super().__init__(name)
        self.vif = interface

    async def body(self):
        """
            The body task that will be run when it is called.
        """
        transaction = VrSeqItem("valid_seq_item", interface=self.vif())
        await self.start_item(transaction)
        transaction.valid = 1
        transaction.valid_rand_en = False
        transaction.randomize()
        await self.finish_item(transaction)
        

class SingleAlwaysReadyRandomSeq(uvm_sequence):
    """
        This is a basic sequence that create Vr sequence item that
        is always ready with random payload. We are not concerned
        about the valid signal since that should be generated by the
        DUT.

        Attributes
        ----------
            vif : Interface
                The target interface.

        Notes
        -----
            See more about uvm_sequence in UVM and pyuvm doc.
    """

    def __init__(self, name, interface):
        """
            Constructor of always ready sequence.

            Parameters
            ----------
                name : str
                    The name of the instance of this sequence.
                interface : Interface
                    An Interface object that stores the Logic information
                    of the targeted interface.
        """
        super().__init__(name)
        self.vif = interface

    async def body(self):
        """
            The body task that will be run when it is called.
        """
        transaction = VrSeqItem("valid_seq_item", interface=self.vif())
        await self.start_item(transaction)
        transaction.ready = 1
        transaction.ready_rand_en = False
        transaction.randomize()
        await self.finish_item(transaction)
        
