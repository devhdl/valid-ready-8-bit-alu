"""
    This file contains the implementation of the
    environment of the testbench of alu block.
"""

from pyuvm import uvm_env
from alu_agent import AluInputAgent, AluOutputAgent
from alu_scoreboard import AluScoreboard


class AluEnv(uvm_env):
    """
        This is a UVM environment built for testing
        the ALU block.
    """

    def build_phase(self):
        """
            This functions build the environment by creating relevant
            agents and scoreboards.
        """
        self.alu_input_agent = AluInputAgent("alu_input_agent", self)
        self.alu_output_agent = AluOutputAgent("alu_output_agent", self)
        self.scoreboard = AluScoreboard("alu_scoreboard", self)

    def connect_phase(self):
        self.alu_input_agent.monitor.aport.connect(self.scoreboard.alu_input_export_fifo.analysis_export)
        self.alu_output_agent.monitor.aport.connect(self.scoreboard.alu_output_export_fifo.analysis_export)

